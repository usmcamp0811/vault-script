{
  description = "Vault Scripts to help standup a vault environment";

  # Nixpkgs / NixOS version to use.
  inputs.nixpkgs.url = "nixpkgs/nixos-21.05";

  outputs = { self, nixpkgs }:
    let

      # to work with older version of flakes
      lastModifiedDate = self.lastModifiedDate or self.lastModified or "19700101";

      # Generate a user-friendly version number.
      version = builtins.substring 0 8 lastModifiedDate;

      # System types to support.
      supportedSystems = [ "x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" ];

      # Helper function to generate an attrset '{ x86_64-linux = f "x86_64-linux"; ... }'.
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;

      # Nixpkgs instantiated for supported system types.
      nixpkgsFor = forAllSystems (system: import nixpkgs { inherit system; overlays = [ self.overlay ]; });

    in
    {
      overlay = final: prev: {
        vpn-init = with final; stdenv.mkDerivation rec {
          name = "vpn-init-${version}";
          unpackPhase = ":";
          buildPhase =
            ''
              cat > vpn-init <<EOF
              #!/bin/bash
              # Check if argument is provided
              if [ -z "\$1" ]; then
                  echo "Usage: $0 <common_name>"
                  exit 1
              fi

              COMMON_NAME=\$1
              # Check if VAULT_ADDR is set
              # if [ -z "\$VAULT_ADDR" ]; then
              #     echo "VAULT_ADDR environment variable is not set. Please set it and try again."
              #     exit 1
              # fi

              # Initialize variables
              ROLE_ID_PATH="/var/lib/vault/$(hostname)/role-id"
              SECRET_ID_PATH="/var/lib/vault/$(hostname)/secret-id"
              TOKEN_PATH="$HOME/.vault-token"

              # Function to issue certificate
              issue_certificate() {
                  output=$(${pkgs.vault} write -format=json ata-pki/issue/vpn-server-role common_name="$COMMON_NAME")
                  echo $(echo $output | ${pkgs.jq}/bin/jq -r '.data.certificate') > server.crt
                  echo $(echo $output | ${pkgs.jq}/bin/jq -r '.data.private_key') > server.key
                  echo $(echo $output | ${pkgs.jq}/bin/jq -r '.data.issuing_ca') > ca.crt
                  echo "Certificate, key, and CA have been saved to server.ct, server.key, and ca.crt."
              }

              # Check if Vault token exists
              if [ -f "$TOKEN_PATH" ]; then
                  VAULT_TOKEN=$(cat "$TOKEN_PATH")
                  issue_certificate "$COMMON_NAME"
                  exit 0

              # Check if role-id and secret-id exist
              elif [ -f "$ROLE_ID_PATH" ] && [ -f "$SECRET_ID_PATH" ]; then
                  role_id=$(cat "$ROLE_ID_PATH")
                  secret_id=$(cat "$SECRET_ID_PATH")
                  VAULT_TOKEN=$(${pkgs.vault} write -field=token auth/approle/login role_id="$role_id" secret_id="$secret_id")
                  issue_certificate "$COMMON_NAME"
                  exit 0
              # Prompt user to login if no existing credentials were found
              else
                echo "No existing Vault credentials found. Please login to Vault."
                ${pkgs.vault}/bin/vault login -no-print
              fi

              if [ \$? -eq 0 ]; then
                  issue_certificate "$COMMON_NAME"
              else
                  echo "Vault login failed. Exiting."
                  exit 1
              fi

              EOF
              chmod +x vpn-init
            '';
          installPhase =
            ''
              mkdir -p $out/bin
              cp vpn-init $out/bin/
            '';

        };
        get-approle-credentials = with final; stdenv.mkDerivation rec {
          name = "get-approle-credentials-${version}";

          unpackPhase = ":";

          buildPhase =
            ''
              cat > get-approle-credentials <<EOF
              #! $SHELL
              # Check that an approle name was provided
              if [ -z "\$1" ]; then
                echo "Usage: get-approle-credentials <approle_name>"
                exit 1
              fi

              # Check if VAULT_ADDR is set
              # if [ -z "$VAULT_ADDR" ]; then
              #   echo "VAULT_ADDR is not set. Exiting."
              #   exit 1
              # fi

              # if [ "$EUID" -ne 0 ]; then
              #   echo "This script must be run as root or with sudo."
              #   exit 1
              # fi

              # Set the approle name
              approle_name=\$1

              # Check if already logged into Vault
              vault_status=$(${pkgs.vault}/bin/vault status -format=json 2>/dev/null)

              if [ $? -eq 0 ]; then
                echo "Already logged into Vault."
              else
                echo "Please login to Vault..."
                ${pkgs.vault}/bin/vault login || { echo "Vault login failed."; exit 1; }
              fi

              # Check that login was successful
              if [ \$? -ne 0 ]; then
                echo "Vault login failed."
                exit 1
              fi

              mkdir -p /var/lib/vault/\$approle_name
              chmod -R 777 /var/lib/vault/\$approle_name

              # Retrieve and save the role-id
              role_id=\$(${pkgs.vault}/bin/vault read -field=role_id auth/approle/role/\$approle_name/role-id)
              echo \$role_id > /var/lib/vault/\$approle_name/role-id

              # Retrieve and save the secret-id
              secret_id=\$(${pkgs.vault}/bin/vault write -f -field=secret_id auth/approle/role/\$approle_name/secret-id)
              echo \$secret_id > /var/lib/vault/\$approle_name/secret-id

              chmod -R 0400 /var/lib/vault/\$approle_name
              echo "AppRole credentials saved to '/var/lib/vault/\$approle_name/role-id' and '/var/lib/vault/\$approle_name/secret-id'."
              EOF
              chmod +x get-approle-credentials
            '';

          installPhase =
            ''
              mkdir -p $out/bin
              cp get-approle-credentials $out/bin/
            '';
        };

      };

      # Provide some binary packages for selected system types.
      packages = forAllSystems (system:
        {
          inherit (nixpkgsFor.${system}) get-approle-credentials;
          inherit (nixpkgsFor.${system}) vpn-init;
        });

      # The default package for 'nix build'. This makes sense if the
      # flake provides only one package or there is a clear "main"
      # package.
      defaultPackage = forAllSystems (system: self.packages.${system}.get-approle-credentials);

      # A NixOS module, if applicable (e.g. if the package provides a system service).
      nixosModules.get-approle-credentials =
        { pkgs, ... }:
        {
          nixpkgs.overlays = [ self.overlay ];

          environment.systemPackages = [ pkgs.get-approle-credentials ];

          #systemd.services = { ... };
        };

      # Tests run by 'nix flake check' and by Hydra.
      checks = forAllSystems
        (system:
          with nixpkgsFor.${system};

          {
            inherit (self.packages.${system}) get-approle-credentials;

            # Additional tests, if applicable.
            test = stdenv.mkDerivation {
              name = "get-approle-credentials-test-${version}";

              buildInputs = [ get-approle-credentials ];

              unpackPhase = "true";

              buildPhase = ''
                echo 'running some integration tests'
                [[ $(get-approle-credentials) = 'Hello Nixers!' ]]
              '';

              installPhase = "mkdir -p $out";
            };
          }

          // lib.optionalAttrs stdenv.isLinux {
            # A VM test of the NixOS module.
            vmTest =
              with import (nixpkgs + "/nixos/lib/testing-python.nix") {
                inherit system;
              };

              makeTest {
                nodes = {
                  client = { ... }: {
                    imports = [ self.nixosModules.get-approle-credentials ];
                  };
                };

                testScript =
                  ''
                    start_all()
                    client.wait_for_unit("multi-user.target")
                    client.succeed("get-approle-credentials")
                  '';
              };
          }
        );

    };
}
